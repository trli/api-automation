package com.salmon.test.runner.rerun;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;


@CucumberOptions(features = "@target/rerun3.txt",plugin = {
        "pretty", "html:target/cucumber-report/runjenkins",
        "json:target/cucumber-report/runjenkinsUI_rerun3/cucumber.json"},
        glue="classpath:com/salmon/test")
public class RerunRunJenkinsUISuite3 extends AbstractTestNGCucumberTests {
}
