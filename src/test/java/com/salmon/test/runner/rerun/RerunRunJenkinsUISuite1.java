package com.salmon.test.runner.rerun;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;


@CucumberOptions(features = "@target/rerun1.txt",plugin = {
        "pretty", "html:target/cucumber-report/runjenkins",
        "json:target/cucumber-report/runjenkinsUI_rerun1/cucumber.json"},
        glue="classpath:com/salmon/test")
public class RerunRunJenkinsUISuite1 extends AbstractTestNGCucumberTests {
}
