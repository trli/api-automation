package com.salmon.test.util;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class StringParser {
    public static Map<String, String> parseMap(String input,String delimiter,String assigner) {
        Map<String, String> map = new HashMap<String, String>();
        for (String pair : input.split(delimiter)) {
            String[] kv = pair.split(assigner);
            map.put(kv[0], kv[1]);
        }
        return map;
    }

    public static String parseAndGetValueOf(String input,String delimiter,String assigner,String key,String[] termsToRemove){
        System.out.println("start parsing");
        System.out.println(input);
        String trimmedInput=input;
        for(String term:termsToRemove){
            System.out.println("removing"+term);
            trimmedInput=trimmedInput.replaceAll(term,"");
        }
        Map<String, String> map = new HashMap<String, String>();
        for (String pair : input.split(delimiter)) {
            String[] kv = pair.split(assigner);
            System.out.println("putting"+kv[1]+" to "+kv[0]);
            map.put(kv[0], kv[1]);
        }
        return map.get(key);
    }
}
