package com.salmon.test.data.web.enums;

public enum PermittedSiteMode {
    DESKTOP,
    MOBILE,
    TABLET
}
