package com.salmon.test.data.api.enums.creat_order;

import java.util.HashMap;
import java.util.Map;

public enum CreateOrderAPI_ExpectedResponse {
    VALID_TOKEN(200,
            new HashMap<String, String>() {{
                put("content-type", "application/json; charset=utf-8");
                put("WWW-Authenticate", "Bearer");
                put("Date", "Wed, 21 Aug 2019 01:48:56 GMT");
                put("charset", "UTF-8");
                put("Server", "Google Frontend");
            }},
            "{\"status\":\"OK\",\"eventId\":\"an2fQ5FOx\",\"ackId\":\"682943838253997\",\"message\":\"Operation successful\"}"),
    INVALID_TOKEN(401,
            new HashMap<String, String>() {{
                put("WWW-Authenticate", "Bearer");
                put("error", "invalid_token");
                put("error_description", "The access token could not be verified");
                put("Date", "Wed, 21 Aug 2019 01:48:56 GMT");
                put("charset", "UTF-8");
                put("Server", "Google Frontend");
                put("Content-Length", "309");
            }},
            "<html><head>\n" +
                    "<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\">\n" +
                    "<title>401 Unauthorized</title>\n" +
                    "</head>\n" +
                    "<body text=#000000 bgcolor=#ffffff>\n" +
                    "<h1>Error: Unauthorized</h1>\n" +
                    "<h2>Your client does not have permission to the requested URL <code>/v1/create-order</code>.<\n" +
                    "/h2>\n" +
                    "<h2></h2>\n" +
                    "</body></html>\n");
    private int statusCode;
    private Map<String,String> headers;
    private String body;
    CreateOrderAPI_ExpectedResponse(int statusCode,Map<String,String> headers,String body){
        this.statusCode=statusCode;
        this.headers=headers;
        this.body=body;
    }

    public int getStatusCode(){
        return statusCode;
    }

    public Map<String,String> getHeaders(){
        return headers;
    }

    public String getBody(){
        return body;
    }
}
