package com.salmon.test.data.api.enums;

public enum ServiceEndPoints {
    CREATE_ORDER("https://tws-dev.wtcde.uk/v1/create-order","POST");
    private String uri;
    private String method;

    ServiceEndPoints(String uri, String method) {
        this.uri=uri;
        this.method=method;
    }

    public String getUri(){
        return uri;
    }

    public String getMethod(){
        return method;
    }
}
