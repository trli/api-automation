package com.salmon.test.data.api.enums.creat_order;

public enum CreateOrderAPI_JsonPath {
    VALID_JSON("src\\test\\resources\\data\\api\\createOrder\\createOrder_valid.json"),
    INVALID_JSON_MISSING_ORDER_NO("src\\test\\resources\\data\\createOrder\\createOrder_valid.json"),;
    private String jsonPath;
    CreateOrderAPI_JsonPath(String jsonPath){
        this.jsonPath=jsonPath;
    }
    public String getJsonPath(){
        return jsonPath;
    }
}
