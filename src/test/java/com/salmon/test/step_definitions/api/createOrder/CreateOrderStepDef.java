package com.salmon.test.step_definitions.api.createOrder;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.auth.oauth2.ServiceAccountJwtAccessCredentials;
import com.jayway.restassured.response.Response;
import com.salmon.test.data.api.enums.creat_order.CreateOrderAPI_ExpectedResponse;
import com.salmon.test.data.api.enums.creat_order.CreateOrderAPI_JsonPath;
import com.salmon.test.data.api.enums.ServiceEndPoints;
import com.salmon.test.services.createOrder.CreateOrderAPI;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.FileInputStream;
import java.security.interfaces.RSAPrivateKey;
import java.util.Date;

import static com.salmon.test.framework.helpers.ApiHelper.getValueInBody;
import static com.salmon.test.framework.helpers.ApiHelper.givenConfig;
import static org.assertj.core.api.Assertions.assertThat;

public class CreateOrderStepDef {
    private static final String saKeyfilePath="C:\\Users\\trli\\SSHKeys\\PrivateKey\\tws-cloudeng-dev-8dabe7d60df5.json";
    private static final String client_id="104220465932620535184";
    private static final String client_email="tws-cloudeng-dev@appspot.gserviceaccount.com";
    private static final String auth_uri="https://accounts.google.com/o/oauth2/auth";
    private static final String private_key_id="private-key-id";
    private static final int expiryLength=900000;

    private static String endPoint= ServiceEndPoints.CREATE_ORDER.getUri();
    private static Response response;
    private final CreateOrderAPI createOrderApi;
    public CreateOrderStepDef(CreateOrderAPI createOrderApi){
        this.createOrderApi=createOrderApi;
    }

    @When("^the user requests for valid token to service$")
    public void the_user_requests_for_valid_token_to_service() throws Exception {
        String token=getSignedJwt();
        System.out.println("jwt:"+token);
    }

    @When("^the user calls create order api using valid json$")
    public void i_call_create_order_api_using_valid_json() {
        try {
            response = givenConfig().body(createOrderApi.getJsonByName(CreateOrderAPI_JsonPath.VALID_JSON.getJsonPath())).post(endPoint);
            System.out.println("status code:"+response.getStatusCode());
            System.out.println("headers:"+response.getHeaders());
            System.out.println("response body:"+response.asString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Then("^the response contains successful create order message$")
    public void theResponseContainsSuccessfulCreateOrderMessage(){
        assertThat(response.getStatusCode()).as("Verify status code").isEqualTo(CreateOrderAPI_ExpectedResponse.VALID_TOKEN.getStatusCode());
        assertThat(response.getHeaders().get("content-type").getValue()).as("Verify error in header").isEqualTo(CreateOrderAPI_ExpectedResponse.VALID_TOKEN.getHeaders().get("content-type"));
        assertThat(getValueInBody(response.asString(),"status")).isEqualTo(getValueInBody(CreateOrderAPI_ExpectedResponse.VALID_TOKEN.getBody(),"status"));
        assertThat(getValueInBody(response.asString(),"message")).isEqualTo(getValueInBody(CreateOrderAPI_ExpectedResponse.VALID_TOKEN.getBody(),"message"));
    }

    private static String getSignedJwt() throws Exception {
        FileInputStream stream = new FileInputStream(saKeyfilePath);
        GoogleCredential cred = GoogleCredential.fromStream(stream);
        RSAPrivateKey key = (RSAPrivateKey) cred.getServiceAccountPrivateKey();

        ServiceAccountJwtAccessCredentials credentials =
                ServiceAccountJwtAccessCredentials.newBuilder()
                        .setClientId(client_id)
                        .setClientEmail(client_email)
                        .setPrivateKey(key)
                        .setPrivateKeyId(private_key_id)
                        .build();
        long now = System.currentTimeMillis();
        RSAPrivateKey privateKey = (RSAPrivateKey) credentials.getPrivateKey();
        Algorithm algorithm = Algorithm.RSA256(null, privateKey);
        return JWT.create()
                .withKeyId(credentials.getPrivateKeyId())
                .withIssuer(credentials.getClientEmail())
                .withSubject(credentials.getClientEmail())
                .withAudience(auth_uri)
                .withIssuedAt(new Date(now))
                .withExpiresAt(new Date(now + expiryLength))
                .withClaim("target_audience", client_id)
                .sign(algorithm);
    }
}
