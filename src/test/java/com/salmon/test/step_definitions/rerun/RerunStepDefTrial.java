package com.salmon.test.step_definitions.rerun;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.salmon.test.framework.helpers.Props;
import com.salmon.test.framework.helpers.rerun.JsonReportHandler;

import cucumber.api.java.en.Given;

public class RerunStepDefTrial {
    /**
     * This is a test to fail intentionally to populate the rerun.txt
     * Otherwise all scenarios will be executed for blank rerun.txt
     */
    @Given("^This is a failed step for first run but passed in rerun$")
    public void this_is_a_failed_step_for_first_run_but_passed_in_rerun() {
        assertThat("failing intentionally", true, equalTo(false));
    }

}
