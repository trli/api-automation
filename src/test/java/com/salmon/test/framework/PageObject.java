package com.salmon.test.framework;

import com.salmon.test.framework.helpers.WebDriverHelper;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public abstract class PageObject {
    @Getter
    private static final long DRIVER_WAIT_TIME = 15;
    private static final Logger LOG = LoggerFactory.getLogger(PageObject.class);

    @Getter
    @Setter
    protected static WebDriverWait wait;
    @Getter
    @Setter
    public static WebDriver webDriver;


    protected PageObject() {
        this.webDriver = WebDriverHelper.getWebDriver();
//        this.wait = new WebDriverWait(webDriver, DRIVER_WAIT_TIME);
    }

    /**
     * Returns the current Url from page
     **/
    public String getCurrentUrl() {
        return webDriver.getCurrentUrl();
    }

    /**
     * Returns the current page title from page
     */
    public String getCurrentPageTitle() {
        return getWebDriver().getTitle();
    }

    /**
     * An expectation for checking the title of a page.
     *
     * @param title the expected title, which must be an exact match
     * @return true when the title matches, false otherwise
     */
    public boolean checkPageTitle(String title) {
        return new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME).until(ExpectedConditions.titleIs(title));
    }

    /**
     * An expectation for checking that the title contains a case-sensitive
     * substring
     *
     * @param title the fragment of title expected
     * @return true when the title matches, false otherwise
     */
    public boolean checkPageTitleContains(String title) {
        return new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME).until(ExpectedConditions.titleContains(title));
    }

    /**
     * An expectation for the URL of the current page to be a specific url.
     *
     * @param url the url that the page should be on
     * @return <code>true</code> when the URL is what it should be
     */
    public boolean checkPageUrlToBe(String url) {
        return new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME).until(ExpectedConditions.urlToBe(url));
    }

    /**
     * An expectation for the URL of the current page to contain specific text.
     *
     * @param fraction the fraction of the url that the page should be on
     * @return <code>true</code> when the URL contains the text
     */
    public boolean checkPageUrlContains(String fraction) {
        return new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME).until(ExpectedConditions.urlContains(fraction));
    }

    /**
     * Expectation for the URL to match a specific regular expression
     *
     * @param regex the regular expression that the URL should match
     * @return <code>true</code> if the URL matches the specified regular expression
     */

    public boolean checkPageUrlMatches(String regex) {
        return new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME).until(ExpectedConditions.urlMatches(regex));
    }

    /**
     * Clear the text in the element
     * Find the dynamic element wait until its visible
     *
     * @param by Element location found by css, xpath, id etc...
     **/
    protected WebElement waitForExpectedElementToClear(final By by) {
        waitForExpectedElement(by).clear();
        return waitForExpectedElement(by);
    }

    /**
     * Find the dynamic element wait until its visible
     *
     * @param by Element location found by css, xpath, id etc...
     **/
    protected WebElement waitForExpectedElement(final By by) {
        return wait.until(visibilityOfElementLocated(by));
    }

    /**
     * Find the dynamic element wait until its visible
     *
     * @param by Element location found by css, xpath, id etc...
     **/
    protected WebElement waitForExpectedElementAfterScrolling(final By by,int pixel) {
        WebElement element=wait.until(visibilityOfElementLocated(by));
        scrollToElement(element);
        jsScrollBy(pixel);
        return element;
    }

    /**
     * Find the first visible element from the list
     *
     **/
    protected WebElement getFirstVisibleElementFromList(final By by) {
       List<WebElement> elements= getWebDriver().findElements(by);
       if(elements.size()==0){
           return null;
       }else{
            for(WebElement e:elements){
                if(e.isDisplayed()){
                    return e;
                }
            }
            return null;
       }
    }

    /**
     * Find the dynamic element wait until its visible for a specified time
     *
     * @param by                Element location found by css, xpath, id etc...
     * @param waitTimeInSeconds max time to wait until element is visible
     **/

    public WebElement waitForExpectedElement(final By by, long waitTimeInSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(getWebDriver(), waitTimeInSeconds);
            return wait.until(visibilityOfElementLocated(by));
        } catch (NoSuchElementException e) {
            LOG.info(e.getMessage());
            return null;
        } catch (TimeoutException e) {
            LOG.info(e.getMessage());
            return null;
        }
    }

    public void clickExpectedElementIfPresent(final By by, long waitTimeInSeconds) {
        try {
            WebDriverWait wait = new WebDriverWait(getWebDriver(), waitTimeInSeconds);
            wait.until(visibilityOfElementLocated(by)).click();
        } catch (Exception e) {
            LOG.info("Element "+by+" not found:"+e.getMessage());
        }
    }

    private ExpectedCondition<WebElement> visibilityOfElementLocated(final By by) throws NoSuchElementException {
        return driver -> {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                LOG.error(e.getMessage());
            }
            WebElement element = getWebDriver().findElement(by);
            return element.isDisplayed() ? element : null;
        };
    }


    /**
     * An expectation for checking if the given text is present in the specified element.
     *
     * @param element the WebElement
     * @param text    to be present in the element
     * @return true once the element contains the given text
     */
    public boolean textToBePresentInElement(WebElement element, String text) {
        return new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME).until(ExpectedConditions.textToBePresentInElement(element, text));
    }


    /**
     * An expectation for checking if the given text is present in the element that matches
     * the given locator.
     *
     * @param by   used to find the element
     * @param text to be present in the element found by the locator
     * @return true once the first element located by locator contains the given text
     */
    public boolean textToBePresentInElementLocated(final By by, final String text) {
        return new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME).until(ExpectedConditions.textToBePresentInElementLocated(by, text));
    }


    /**
     * An expectation for checking if the given text is present in the specified
     * elements value attribute.
     *
     * @param element the WebElement
     * @param text    to be present in the element's value attribute
     * @return true once the element's value attribute contains the given text
     */
    public boolean textToBePresentInElementValue(final WebElement element, final String text) {
        return new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME).until(ExpectedConditions.textToBePresentInElementValue(element, text));
    }


    /**
     * An expectation for checking if the given text is present in the specified
     * elements value attribute.
     *
     * @param by   used to find the element
     * @param text to be present in the value attribute of the element found by the locator
     * @return true once the value attribute of the first element located by locator contains
     * the given text
     */
    public boolean textToBePresentInElementValue(final By by, final String text) {
        return new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME).until(ExpectedConditions.textToBePresentInElementValue(by, text));
    }


    /**
     * An expectation for checking whether the given frame is available to switch
     * to. <p> If the frame is available it switches the given driver to the
     * specified frame.
     *
     * @param frameLocator used to find the frame (id or name)
     */
    public WebDriver frameToBeAvailableAndSwitchToIt(final String frameLocator) {
        return new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator));
    }


    /**
     * An expectation for checking whether the given frame is available to switch
     * to. <p> If the frame is available it switches the given driver to the
     * specified frame.
     *
     * @param by used to find the frame
     */
    public WebDriver frameToBeAvailableAndSwitchToIt(final By by) {
        return new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(by));
    }

    /**
     * An expectation for checking whether the given frame is available to switch
     * to. <p> If the frame is available it switches the given driver to the
     * specified frame.
     *
     * @param by used to find the frame
     */
    public WebDriver frameToBeAvailableAndSwitchToIt(final By by,Long seconds) {
        return new WebDriverWait(getWebDriver(), seconds).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(by));
    }


    /**
     * An expectation for checking that an element is either invisible or not
     * present on the DOM.
     *
     * @param by used to find the element
     */
    public boolean invisibilityOfElementLocated(By by) {
        return (new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME)).until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    /**
     * An expectation for checking that an element with text is either invisible
     * or not present on the DOM.
     *
     * @param by   used to find the element
     * @param text of the element
     */
    public boolean invisibilityOfElementWithText(final By by, final String text) {
        return (new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME)).until(ExpectedConditions.invisibilityOfElementWithText(by, text));
    }


    /**
     * An expectation for checking an element is visible and enabled such that you
     * can click it.
     *
     * @param by used to find the element
     * @return the WebElement once it is located and clickable (visible and enabled)
     */
    public WebElement elementToBeClickable(By by) {
        return (new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME)).until(ExpectedConditions.elementToBeClickable(by));
    }


    /**
     * An expectation for checking an element is visible and enabled such that you
     * can click it.
     *
     * @param element the WebElement
     * @return the (same) WebElement once it is clickable (visible and enabled)
     */

    public WebElement elementToBeClickable(final WebElement element) {
        return (new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME)).until(ExpectedConditions.elementToBeClickable(element));
    }


    /**
     * Wait until an element is no longer attached to the DOM.
     *
     * @param element The element to wait for.
     * @return false is the element is still attached to the DOM, true
     * otherwise.
     */
    public boolean stalenessOf(final WebElement element) {
        return (new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME)).until(ExpectedConditions.stalenessOf(element));
    }

    /**
     * An expectation for checking if the given element is selected.
     */
    public boolean elementToBeSelected(final By by) {
        return (new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME)).until(ExpectedConditions.elementToBeSelected(by));
    }

    /**
     * An expectation for checking if the given element is selected.
     */
    public boolean elementToBeSelected(final WebElement element) {
        return (new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME)).until(ExpectedConditions.elementToBeSelected(element));
    }

    /**
     * An expectation for checking if the given element is selected.
     */
    public boolean elementSelectionStateToBe(final WebElement element, final boolean selected) {
        return (new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME)).until(ExpectedConditions.elementSelectionStateToBe(element, selected));
    }

    /**
     * An expectation for checking if the given element is selected.
     */
    public boolean elementSelectionStateToBe(final By by,
                                             final boolean selected) {
        return (new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME)).until(ExpectedConditions.elementSelectionStateToBe(by, selected));
    }

    public void waitForAlert() {
        (new WebDriverWait(getWebDriver(), 2)).until(ExpectedConditions.alertIsPresent());
    }

    /**
     * An expectation for checking that all elements present on the web page that
     * match the locator are visible. Visibility means that the elements are not
     * only displayed but also have a height and width that is greater than 0.
     *
     * @param by used to find the element
     * @return the list of WebElements once they are located
     */
    public List<WebElement> visibilityOfAllElementsLocatedBy(final By by) {
        return (new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME)).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
    }


    /**
     * An expectation for checking that all elements present on the web page that
     * match the locator are visible. Visibility means that the elements are not
     * only displayed but also have a height and width that is greater than 0.
     *
     * @param elements list of WebElements
     * @return the list of WebElements once they are located
     */
    public List<WebElement> visibilityOfAllElements(final List<WebElement> elements) {
        return (new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME)).until(ExpectedConditions.visibilityOfAllElements(elements));
    }


    /**
     * An expectation for checking that there is at least one element present on a
     * web page.
     *
     * @param by used to find the element
     * @return the list of WebElements once they are located
     */
    public List<WebElement> presenceOfAllElementsLocatedBy(final By by) {
        return (new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
    }

    /**
     * An expectation for checking that an element, known to be present on the DOM
     * of a page, is visible. Visibility means that the element is not only
     * displayed but also has a height and width that is greater than 0.
     *
     * @param element the WebElement
     * @return the (same) WebElement once it is visible
     */

    public WebElement visibilityOf(final WebElement element) {
        return (new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME)).until(ExpectedConditions.visibilityOf(element));
    }


    /**
     * An expectation for checking that an element is present on the DOM of a
     * page. This does not necessarily mean that the element is visible.
     *
     * @param by used to find the element
     * @return the WebElement once it is located
     */
    public boolean isElementPresent(final By by) {
        try {
            new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME).until(ExpectedConditions.presenceOfElementLocated(by));

        } catch (TimeoutException exception) {
            //LOG.info(exception.getMessage());
            return false;
        }
        return true;
    }

    /**
     * An expectation for checking that an element is present on the DOM of a
     * page. This does not necessarily mean that the element is visible.
     *
     * @param by used to find the element
     * @return the WebElement once it is located
     */
    public boolean isElementPresent(final By by,int seconds) {
        try {
            new WebDriverWait(getWebDriver(), seconds).until(ExpectedConditions.presenceOfElementLocated(by));

        } catch (TimeoutException exception) {
            //LOG.info(exception.getMessage());
            return false;
        }
        return true;
    }

    protected boolean isElementDisplayed(final By by) {
        try {
            new WebDriverWait(getWebDriver(), DRIVER_WAIT_TIME).until(ExpectedConditions.visibilityOfElementLocated(by));
        } catch (TimeoutException exception) {
            //LOG.info(exception.getMessage());
            return false;
        }
        return true;
    }

    public boolean isElementDisplayedBySeconds(final By by,int seconds) {
        try {
            new WebDriverWait(getWebDriver(), seconds).until(ExpectedConditions.visibilityOfElementLocated(by));
        } catch (TimeoutException exception) {
            //LOG.info(exception.getMessage());
            return false;
        }
        return true;
    }

    public WebDriver getBrowserByPageTitle(String pageTitle) {
        for (String windowHandle : webDriver.getWindowHandles()) {
            webDriver = webDriver.switchTo().window(windowHandle);
            if (pageTitle.equalsIgnoreCase(webDriver.getTitle())) {
                return webDriver;
            }
        }
        return null;
    }

    public WebDriver getBrowserByPartialPageTitle(String pageTitle) {
        for (String windowHandle : webDriver.getWindowHandles()) {
            webDriver = webDriver.switchTo().window(windowHandle);
            if (webDriver.getTitle().contains(pageTitle)) {
                return webDriver;
            }
        }
        return null;
    }


    public void navigateToPreviousPageUsingBrowserBackButton() {
        webDriver.navigate().back();
    }

    public void clickWithinElementWithXYCoordinates(WebElement webElement, int x, int y) {
        Actions builder = new Actions(webDriver);
        builder.moveToElement(webElement, x, y);
        builder.click();
        builder.perform();
    }

    public void moveToElementToClick(WebElement element) {
        Actions builder = new Actions(webDriver);
        builder.moveToElement(element);
        builder.click();
        builder.perform();
    }

    public String getElementByTagNameWithJSExecutor(String tagName) {
        return ((JavascriptExecutor) webDriver).executeScript("return window.getComputedStyle(document.getElementsByTagName('" + tagName + "')").toString();
    }

    public String getElementByQueryJSExecutor(String cssSelector) {
        return ((JavascriptExecutor) webDriver).executeScript("return window.getComputedStyle(document.querySelector('" + cssSelector + "')").toString();
    }

    /**
     * click an element with less chance of failing due to "unclickable" element
     *
     * @param cssSelector
     */
    public void clickElement(By cssSelector) {
        try {
            waitForExpectedElement(cssSelector).click();
        } catch (TimeoutException te) {
            LOG.info("TimeoutException. Cannot click selector: " + cssSelector.toString() + te.getMessage());
            try {
                JavascriptExecutor jse = (JavascriptExecutor) this.webDriver;
                jse.executeScript("arguments[0].click();", element(cssSelector));
            } catch (TimeoutException te2) {
                LOG.info("Cannot click selector using JavaScriptExecutor either");
                throw new TimeoutException();
            }
        }
    }

    /**
     * click the first working option from multiple possible options for a part of the page
     *
     */
    public void clickElement(ArrayList<By> allPossibleElements) {
        for (int i = 0; i < allPossibleElements.size(); i++) {
            By cssSelector = allPossibleElements.get(i);
            try {
                clickElement(cssSelector);
                break;
            } catch (Exception e) {
                LOG.info("Exception when trying selector: " + cssSelector.toString() + e.getMessage());
            }
        }
    }

    protected void clickFirstVisibleElement(List<WebElement> elements) {
        for (WebElement element : elements) {
            if (element.isDisplayed()) {
                try {
                    element.click();
                    break;
                } catch (Exception e) {
                    LOG.info("Exception in clickFirstVisibleElement: " + e.getMessage());
                }
            }
        }
    }

    /**
     * Get Composite CSS Selector for a list of element selectors
     *
     * @param cssSelectors
     * @return
     */
    public String getCompositeCssSelector(String[] cssSelectors) {
        StringBuffer compositeCssSelector = new StringBuffer();

        for (String cssSelector : cssSelectors) {
            compositeCssSelector.append(cssSelector.trim() + " ");
        }

        return compositeCssSelector.toString().trim();
    }

    /**
     * info like basket contents are saved from previous tests so needs to be cleared
     */
    public void clearLocalSessionStorage() {
        JavascriptExecutor jse = (JavascriptExecutor) this.webDriver;
        jse.executeScript(String.format("window.localStorage.clear();"));
    }

    public void jsScrollBy(int offset) {
        ((JavascriptExecutor) this.webDriver).executeScript("window.scrollBy(0, " + offset + ")", "");
    }

    public ArrayList<String> getElements(String webElement) {
        ArrayList<String> breakDownElements = new ArrayList<String>();

        waitForExpectedElement(By.xpath(webElement));

        List<WebElement> list = webDriver.findElements(By.xpath(webElement));
        for (WebElement element : list) {
            breakDownElements.add(element.getText());
        }

        return breakDownElements;
    }

    public List<WebElement> getElements(By by) {
        isElementPresent(by,5);
        return webDriver.findElements(by);
    }

    public void sleepMillis(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickUsingJS(By by)
    {
        WebElement element = getWebDriver().findElement(by);
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
    }

    public void clickUsingJS(WebElement element)
    {
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
    }

    public void jsScrollElementInCenter(By by) {
        WebElement element = waitForExpectedElement(by);
        String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
        ((JavascriptExecutor) getWebDriver()).executeScript(scrollElementIntoMiddle, element);
    }

    public void jsScrollIntoView(By by) {
        JavascriptExecutor js = (JavascriptExecutor) getWebDriver();
        js.executeScript("arguments[0].scrollIntoView();", waitForExpectedElement(by));
    }

    private ExpectedCondition<Boolean> isPageLoaded() {
        return driver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
    }

    public void waitForPageLoad() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(isPageLoaded());
    }

    public void clickWithRetry(By by, int retryCount) {
        boolean isClicked = false;
        int retry = 0;
        while (!isClicked && retry < retryCount) {
            try {
                waitForExpectedElement(by).click();
                isClicked = true;
            } catch (Exception e) {
                LOG.warn("Not clicked successfully. Now retry again with time: " + (++retry) + " to click element by " + by);
                isClicked = false;
            } finally {
                if (!isClicked) {
                    continue;
                }
                LOG.info("The element is clicked finally: " + by.toString());
                isClicked = true;
            }
        }
    }

    public void selectOptionByText(WebElement selector, String text) {
        Select s = new Select(selector);
        List<WebElement> allOptions = s.getOptions();
        for (WebElement opt : allOptions) {
            if (opt.getText().trim().equals(text)) {
                LOG.info("Select the option: " + text + " from selector.");
                s.selectByVisibleText(opt.getText());
                break;
            }
        }
    }

    public void scrollToElement(WebElement element){
        Actions action = new Actions(WebDriverHelper.getWebDriver());
        action.moveToElement(element).build().perform();
    }

    protected WebElement element(final By by) {
        return getWebDriver().findElement(by);
    }

    public void waitWhileElementIsPresent(By by,int maxWaitTimeSeconds,Long frequencyInSec) {
        int timeout = maxWaitTimeSeconds;
        while(isElementPresent(by,frequencyInSec)) {
            if(timeout > 0) {
                timeout--;
                try {
                    Thread.sleep(1000);
                } catch(Exception e) {
                    LOG.error("sleep interrupted");
                }
            }
        }
    }

    public boolean isElementPresent(By by,long maxWaitTimeInSeconds) {
        try {
            new WebDriverWait(getWebDriver(), maxWaitTimeInSeconds).until(ExpectedConditions.presenceOfElementLocated(by));

        } catch (TimeoutException exception) {
            //LOG.info(exception.getMessage());
            return false;
        }
        return true;
    }

    public void switchToDefaultContentFrame(){
        webDriver.switchTo().defaultContent();
    }

    public String getCookieContent(String cookieName) {
        return webDriver.manage().getCookieNamed(cookieName).getValue();
    }

    public String getShopLanguage() {
        return webDriver.manage().getCookieNamed("APP_C_LANGUAGE").getValue();
    }

    public boolean isAttributePresent(By selector,String attribute) {
        WebElement element=waitForExpectedElement(selector);
        Boolean result = false;
        try {
            String value = element.getAttribute(attribute);
            if (value != null){
                result = true;
            }
        } catch (Exception e) {LOG.error("fail to get attribute");}

        return result;
    }

    public void setGeolocation(String latitude,String longitude){
        ((JavascriptExecutor) PageObject.getWebDriver()).executeScript("window.navigator.geolocation.getCurrentPosition=function(success){"+
                "var position = {\"coords\" : {\"latitude\": \""+latitude+"\",\"longitude\": \"1"+longitude+"\"}};"+
                "success(position);}");

        System.out.println(((JavascriptExecutor) PageObject.getWebDriver()).executeScript("var positionStr=\"\";"+
                "window.navigator.geolocation.getCurrentPosition(function(pos){positionStr=pos.coords.latitude+\":\"+pos.coords.longitude});"+
                "return positionStr;"));
    }

    public boolean isElementDisplayedWithinElement(WebElement webElement,By by) {
        try {
            webElement.findElement(by);
        } catch (NoSuchElementException exception) {
            return false;
        }
        return true;
    }


    /**
     * Set iFrame to web driver to pick element from this iframe
     *
     */
    public static void setIFrame(String iFrame) {
        WebElement frame = WebDriverHelper.getWebDriver().findElement(By.cssSelector(iFrame));
        WebDriverHelper.getWebDriver().switchTo().frame(frame);
    }

    /**
     * Set iFrame to web driver to pick element from this iframe
     *
     */
    public static void setIFrame(By iFrame) {
        WebElement frame = WebDriverHelper.getWebDriver().findElement(iFrame);
        WebDriverHelper.getWebDriver().switchTo().frame(frame);
    }

    /**
     * Set iFrame to web driver to pick element from this iframe if present
     *
     */
    public static void setIFrameIfPresent(String iFrame) {
        try{
            WebElement frame = WebDriverHelper.getWebDriver().findElement(By.cssSelector(iFrame));
            WebDriverHelper.getWebDriver().switchTo().frame(frame);
        }catch(Exception e){
            LOG.info("Fail to switch to iframe "+iFrame);
        }
    }

    /**
     * Set iFrame to web driver to pick element from this iframe if present
     *
     */
    public static void setIFrameIfPresent(By iFrame) {
        try{
            WebElement frame = WebDriverHelper.getWebDriver().findElement(iFrame);
            WebDriverHelper.getWebDriver().switchTo().frame(frame);
        }catch(Exception e){
            LOG.info("Fail to switch to iframe "+iFrame);
        }
    }

}