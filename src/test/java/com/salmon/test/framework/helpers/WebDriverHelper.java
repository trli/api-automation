package com.salmon.test.framework.helpers;

import com.salmon.test.data.web.enums.PermittedSiteMode;
import com.salmon.test.framework.PageObject;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static org.openqa.selenium.ie.InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS;

public class WebDriverHelper extends EventFiringWebDriver {
    private static final Logger LOG = LoggerFactory.getLogger(WebDriverHelper.class);
    private static final Dimension BROWSER_WINDOW_SIZE = new Dimension(1920, 1024);
    private static final Dimension MOBILE_WINDOW_SIZE = new Dimension(375, 1024);
    private static final Dimension TABLET_WINDOW_SIZE = new Dimension(768, 1024);
    public static RemoteWebDriver REAL_DRIVER = null;
    private static String VENDOR;
    private static final Thread CLOSE_THREAD = new Thread() {
        @Override
        public void run() {
            if (REAL_DRIVER != null) {
                REAL_DRIVER.quit();
            }
        }
    };

    public WebDriverHelper() {
        super(REAL_DRIVER);
    }

    private static String BROWSER;
    private static String PLATFORM;
    private static String DRIVER_PATH;
    private static String DRIVER_ROOT_DIR;
    private static String FILE_SEPARATOR;
    private static String SELENIUM_HOST;
    private static String SELENIUM_PORT;
    private static String SELENIUM_REMOTE_URL;
    private static String DEVICE_NAME;
    private static String DEVICE_PLATFORM;
    private static String DEVICE_BROWSER;
    private static String USER_TOKEN;
    private static String UDID;
    @Getter
    @Setter
    public static boolean changeBrowserLanguage = false;
    @Getter
    @Setter
    public static String browserLanguage = "en-US";
    @Getter
    @Setter
    public static boolean incognitoRequired = false;

    static {
        SELENIUM_HOST = System.getProperty("driverhost");
        SELENIUM_PORT = System.getProperty("driverport");
        FILE_SEPARATOR = System.getProperty("file.separator");
        PLATFORM = Props.getProp("platform");
        BROWSER = Props.getProp("browser");
        DRIVER_ROOT_DIR = Props.getProp("driver.root.dir");
        VENDOR = System.getProperty("vendor");
        //VENDOR = Props.getProp("vendor");
        USER_TOKEN = Props.getProp("user_token");
        DEVICE_NAME = System.getProperty("device", Props.getProp("motel_device"));
        DEVICE_PLATFORM = System.getProperty("platform", Props.getProp("motel_platform"));
        DEVICE_BROWSER = System.getProperty("browser", Props.getProp("motel_browser"));
        UDID = Props.getProp("motel.udid");

        if (!BROWSER.toLowerCase().equals("safari")) {
            System.setProperty("webdriver.chrome.driver", getDriverPath());
            System.setProperty("webdriver.ie.driver", getDriverPath());
            System.setProperty("phantomjs.binary.path", getDriverPath());
            System.setProperty("webdriver.gecko.driver", getDriverPath());
        }

        Runtime.getRuntime().addShutdownHook(CLOSE_THREAD);
    }

    public static WebDriver getWebDriver() {
        return REAL_DRIVER;
    }

    public static WebDriver getSpecificWebDriver(PermittedSiteMode mode) {
        if (BROWSER.equalsIgnoreCase("chrome")) {
            if (mode.equals(PermittedSiteMode.DESKTOP)) {
                startChromeDriver();
                REAL_DRIVER.manage().window().setSize(BROWSER_WINDOW_SIZE);
            }
            else if (mode.equals(PermittedSiteMode.MOBILE)) {
                if (VENDOR != null && VENDOR.equalsIgnoreCase("MoTel")) {
                    startAppiumMoTelDriver(mode);
                } else {
                    startChromeDriver();
                    REAL_DRIVER.manage().window().setSize(MOBILE_WINDOW_SIZE);
                }
            }
        } else if (BROWSER.equalsIgnoreCase("firefox")) {
            if (mode.equals(PermittedSiteMode.DESKTOP)) {
                startSpecificFireFoxDriver();
            } else {
                startAppiumMoTelDriver(mode);
            }
        } else if (BROWSER.equalsIgnoreCase("iexplore")) {
            startIEDriver();
        } else {
            throw new IllegalArgumentException("Browser type not supported: " + System.getProperty("browserName"));
        }
        LOG.info("WebDriver start up finished.");
        switch (mode) {
            case DESKTOP:
                REAL_DRIVER.manage().window().setSize(BROWSER_WINDOW_SIZE);
                break;
            case TABLET:
                REAL_DRIVER.manage().window().setSize(TABLET_WINDOW_SIZE);
                break;
            default:
                break;
        }

        REAL_DRIVER.manage().timeouts().pageLoadTimeout(2, TimeUnit.MINUTES);
        //REAL_DRIVER.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        PageObject.setWebDriver(REAL_DRIVER);
        PageObject.setWait(new WebDriverWait(REAL_DRIVER, PageObject.getDRIVER_WAIT_TIME()));
        return REAL_DRIVER;
    }

    private static void startAppiumMoTelDriver(PermittedSiteMode mode) {
        DesiredCapabilities capabilities = getMoTelDesiredCapabilities();
        System.out.println("XXXXXXXXXXXXXXXXXXXXXXX" + capabilities);
        if (VENDOR.equalsIgnoreCase("MoTel")) {
            if (REAL_DRIVER != null) {
                return;
            }
            try {
                REAL_DRIVER = new RemoteWebDriver(new URL("https://portal.motel.io/wd/hub"), capabilities);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

    }

    public static Platform getPlatform() {
        Platform platform = null;
        switch (DEVICE_PLATFORM) {
            case "ANDROID":
                platform = Platform.ANDROID;
                break;
            case "IOS":
                platform = Platform.IOS;
                break;
        }
        return platform;

    }

    public static String setDeviceName() {
        String setDeviceName = null;
        switch (DEVICE_NAME) {
            case "samsungS6":
                setDeviceName = "04157df4cb95971a";
                break;
            case "samsungS6Edge":
                setDeviceName = "1215fca29a852904";
                break;
            case "samsungS7":
                setDeviceName = "ad0617020f2fcea9e3";
                break;
            case "Samsung Galaxy S8":
                setDeviceName = "ce02171251ba2f1e05";
                break;
            case "iPhone 6 Plus":
                setDeviceName = "8cfab6e77f1200a1046968e562ebdc3dd7a8fdb7";
                break;
            case "iPhoneX":
                setDeviceName = "77b59313addbcab3ef3f1ebb1741815c6fbb34f8";
                break;
            case "iPhone 6":
                setDeviceName = "d20cbfe1c490fa91819bd4ab035903b429ac5eee";
                break;
            case "motoG":
                setDeviceName = "TA93301X9X";
                break;

        }
        return setDeviceName;

    }

    private static DesiredCapabilities getMoTelDesiredCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("newCommandTimeout", 240);
        capabilities.setPlatform(getPlatform());
        capabilities.setCapability("browserName", DEVICE_BROWSER);
        capabilities.setCapability("udid", setDeviceName());
        capabilities.setCapability("token", USER_TOKEN);
        capabilities.setCapability("udid", UDID);
        capabilities.setCapability("unicodeKeyboard", true);
        capabilities.setCapability("resetKeyboard", true);
        return capabilities;
    }

    private static String getDriverPath() {
        if (BROWSER.equals("chrome") && PLATFORM.contains("win")) {
            DRIVER_PATH = "tools" + FILE_SEPARATOR + "chromedriver"
                    + FILE_SEPARATOR + PLATFORM + FILE_SEPARATOR
                    + "chromedriver.exe";
        } else if (BROWSER.equals("chrome") && PLATFORM.contains("linux")) {
            DRIVER_PATH = DRIVER_ROOT_DIR + FILE_SEPARATOR + "chromedriver"
                    + FILE_SEPARATOR + PLATFORM + FILE_SEPARATOR
                    + "chromedriver";
        } else if (BROWSER.equals("chrome") && PLATFORM.contains("mac")) {
            DRIVER_PATH = DRIVER_ROOT_DIR + FILE_SEPARATOR + "chromedriver"
                    + FILE_SEPARATOR + PLATFORM + FILE_SEPARATOR
                    + "chromedriver";
        } else if (BROWSER.equals("iexplore") && PLATFORM.contains("win")) {
            DRIVER_PATH = DRIVER_ROOT_DIR + FILE_SEPARATOR + "iedriver"
                    + FILE_SEPARATOR + PLATFORM + FILE_SEPARATOR
                    + "IEDriverServer.exe";
        } else if (BROWSER.equals("phantomjs") && PLATFORM.contains("linux")) {
            DRIVER_PATH = DRIVER_ROOT_DIR + FILE_SEPARATOR + "phantomjs"
                    + FILE_SEPARATOR + PLATFORM + FILE_SEPARATOR
                    + "phantomjs";
        } else if (BROWSER.equals("phantomjs") && PLATFORM.contains("win")) {
            DRIVER_PATH = DRIVER_ROOT_DIR + FILE_SEPARATOR + "phantomjs"
                    + FILE_SEPARATOR + PLATFORM + FILE_SEPARATOR
                    + "phantomjs.exe";
        } else if (BROWSER.equals("firefox")) {
            DRIVER_PATH = "tools" + FILE_SEPARATOR + "geckodriver"
                    + FILE_SEPARATOR + PLATFORM + FILE_SEPARATOR
                    + "geckodriver.exe";
        }
        return DRIVER_PATH;
    }

    private static void startIEDriver() {
        InternetExplorerOptions capabilities = getInternetExploreDesiredCapabilities();
        if (SELENIUM_HOST == null)
            REAL_DRIVER = new InternetExplorerDriver(capabilities);
        else {
            try {
                REAL_DRIVER = getRemoteWebDriver(capabilities);
            } catch (MalformedURLException e) {
                LOG.error(SELENIUM_REMOTE_URL + " Error " + e.getMessage());
            }
        }
    }

    private static void startSpecificFireFoxDriver() {
        FirefoxOptions options = getFireFoxOptions();
        if (SELENIUM_HOST == null || SELENIUM_HOST == "") {
            LOG.info("\n Executing Tests Locally on Firefox \n");
            try {
                REAL_DRIVER = new FirefoxDriver(options);
            } catch (Exception e) {
                LOG.error(SELENIUM_REMOTE_URL + " Error " + e.getMessage());
            }
        } else {
            try {
                REAL_DRIVER = getRemoteWebDriver(options);
            } catch (MalformedURLException e) {
                LOG.error(SELENIUM_REMOTE_URL + " Error " + e.getMessage());
            }
        }
    }

    protected static void startChromeDriver() {
        ChromeOptions options = getChromeDesiredCapabilities();
        if (REAL_DRIVER != null) {
            return;
        }
        if (SELENIUM_HOST == null || SELENIUM_HOST == "") {
            LOG.info("\n Executing Tests Locally on chrome. \n");
            REAL_DRIVER = new ChromeDriver(options);
        } else {
            try {
                LOG.info("\n Executing Tests on  Remote WebDriver on Chrome \n");
                REAL_DRIVER = getRemoteWebDriver(options);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    private static ChromeOptions getChromeDesiredCapabilities() {
        LoggingPreferences logs = new LoggingPreferences();
        logs.enable(LogType.DRIVER, Level.OFF);

        ChromeOptions chromeOptions = new ChromeOptions();
        //chromeOptions.addArguments("--test-type");
        chromeOptions.addArguments("--disable-web-security");
        //trli added on 24th April 2019 to resolve "DevToolsActivePort file doesn’t exist." issue.
        chromeOptions.addArguments("–no-sandbox");
        chromeOptions.addArguments("–disable-dev-shm-usage");
        chromeOptions.setExperimentalOption("useAutomationExtension", false);
        /********************/
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("intl.accept_languages", browserLanguage);
        //enable geo emulation, 1:allow 2:block
        prefs.put("profile.default_content_setting_values.geolocation", 1);
        chromeOptions.setExperimentalOption("prefs", prefs);
        chromeOptions.setCapability(CapabilityType.PROXY, new Proxy().setProxyType(Proxy.ProxyType.DIRECT));
        return chromeOptions;
    }

    private static FirefoxOptions getFireFoxOptions() {
        FirefoxOptions options = new FirefoxOptions();
        FirefoxProfile profile = new FirefoxProfile();
        options.setCapability(FirefoxDriver.PROFILE, profile);
        options.setCapability("marionette", true);
        options.setCapability("platform", "WINDOWS");
        //options.setBrowserName("firefox");
        options.setCapability("acceptInsecureCerts", true);
        return options;
    }

    private static InternetExplorerOptions getInternetExploreDesiredCapabilities() {
        LoggingPreferences logs = new LoggingPreferences();
        logs.enable(LogType.DRIVER, Level.OFF);

        InternetExplorerOptions options = new InternetExplorerOptions();
        options.setCapability(INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);

        return options;
    }

    private static DesiredCapabilities getAppiumDesiredCapabilities() {
        File classpathRoot = new File(System.getProperty("user.dir"));
        File appDir = new File(classpathRoot, "../../../apps/ApiDemos/bin");
        File app = new File(appDir, "ApiDemos-debug.apk");

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
        capabilities.setCapability("deviceName", "Android Emulator");
        capabilities.setCapability("platformVersion", "4.4");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability("appPackage", "com.example.android.apis");
        capabilities.setCapability("appActivity", ".ApiDemos");
        return capabilities;
    }

    private static RemoteWebDriver getRemoteWebDriver(DesiredCapabilities capabilities) throws MalformedURLException {

        SELENIUM_REMOTE_URL = "http://" + SELENIUM_HOST + ":" + SELENIUM_PORT + "/wd/hub";
        LOG.info(SELENIUM_REMOTE_URL + " Checking Selenium Remote URL");
        return new RemoteWebDriver(new URL(SELENIUM_REMOTE_URL), (capabilities));
    }

    private static RemoteWebDriver getRemoteWebDriver(ChromeOptions options) throws MalformedURLException {
        SELENIUM_REMOTE_URL = "http://" + SELENIUM_HOST + ":" + SELENIUM_PORT + "/wd/hub";
        LOG.info(SELENIUM_REMOTE_URL + " Checking Selenium Remote URL");
        return new RemoteWebDriver(new URL(SELENIUM_REMOTE_URL), (options));
    }

    private static RemoteWebDriver getRemoteWebDriver(InternetExplorerOptions options) throws MalformedURLException {
        SELENIUM_REMOTE_URL = "http://" + SELENIUM_HOST + ":" + SELENIUM_PORT + "/wd/hub";
        LOG.info(SELENIUM_REMOTE_URL + " Checking Selenium Remote URL");
        return new RemoteWebDriver(new URL(SELENIUM_REMOTE_URL), (options));
    }

    private static RemoteWebDriver getRemoteWebDriver(FirefoxOptions options) throws MalformedURLException {
        SELENIUM_REMOTE_URL = "http://" + SELENIUM_HOST + ":" + SELENIUM_PORT + "/wd/hub";
        LOG.info(SELENIUM_REMOTE_URL + " Checking Selenium Remote URL");
        return new RemoteWebDriver(new URL(SELENIUM_REMOTE_URL), (options));
    }

    private static RemoteWebDriver getRemoteWebDriver(SafariOptions options) throws MalformedURLException {
        SELENIUM_REMOTE_URL = "http://" + SELENIUM_HOST + ":" + SELENIUM_PORT + "/wd/hub";
        LOG.info(SELENIUM_REMOTE_URL + " Checking Selenium Remote URL");
        return new RemoteWebDriver(new URL(SELENIUM_REMOTE_URL), (options));
    }

    @Override
    public void close() {

        LOG.info("CLOSE STARTED");
        if (Thread.currentThread() != CLOSE_THREAD) {
            throw new UnsupportedOperationException(
                    "You shouldn't close this WebDriver. It's world and will close when the JVM exits.");
        }
        super.close();
        LOG.info("CLOSE FINISHED");
    }
}
