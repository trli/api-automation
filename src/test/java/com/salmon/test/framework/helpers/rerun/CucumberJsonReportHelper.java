package com.salmon.test.framework.helpers.rerun;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.salmon.test.framework.helpers.Props;

public class CucumberJsonReportHelper {
	private static final Logger LOG = LoggerFactory.getLogger(CucumberJsonReportHelper.class);
	private static String reportImageExtension = "png";
	
	public static void mergeAndCleanRerunJsonReport() throws FileNotFoundException {
		LOG.info("In after suite method");
		List<File> originalJsonFiles=getOriginalJsonFiles("target/cucumber-report", "json", "cucumber.json");
		List<File> rerunJsonFiles=getRerunJsonFiles("target/cucumber-report", "json", "cucumber.json");
		for(int i=0;i<originalJsonFiles.size();i++){
			mergeRerunJsonReportToOriginalReport(rerunJsonFiles.get(i).getAbsolutePath(), originalJsonFiles.get(i).getAbsolutePath());
		}		
		clearJsonFiles(rerunJsonFiles);		
    }
	
	public static void clearJsonFiles(List<File> files)  {
		for(File file:files){
			LOG.info("Rerun json report removed:"+file.getAbsolutePath());
			file.deleteOnExit();
		}			
	}	
	
	/**
     * Return list of files whose path does NOT contains case-insentive keyword 'rerun'
     * Then have the files reordered by path name
     */
	public static List<File> getOriginalJsonFiles(String reportDirectory, String format, String reportFileName){
		Collection<File> existingReports = FileUtils.listFiles(new File(reportDirectory), new String[]{format}, true);
		List<File> filteredFiles = existingReports.stream().filter(i -> !i.getAbsolutePath().toLowerCase().contains("rerun")).collect(Collectors.toList());
		filteredFiles=filteredFiles.stream().sorted((o1, o2)->o1.getAbsolutePath().compareTo(o2.getAbsolutePath())). collect(Collectors.toList());
		for(File file:filteredFiles){
			LOG.info("Retrieving original files:"+file.getAbsolutePath()+" , "+file.length());
		}	
		return (List<File>) filteredFiles;
	}
	
	/**
     * Return list of files whose path contains case-insentive keyword 'rerun'
     * Then have the files reordered by path name
     */
	public static List<File> getRerunJsonFiles(String reportDirectory, String format, String reportFileName){
		Collection<File> existingReports = FileUtils.listFiles(new File(reportDirectory), new String[]{format}, true);
		List<File> filteredFiles = existingReports.stream().filter(i -> i.getAbsolutePath().toLowerCase().contains("rerun")).collect(Collectors.toList());
		filteredFiles=filteredFiles.stream().sorted((o1, o2)->o1.getAbsolutePath().compareTo(o2.getAbsolutePath())). collect(Collectors.toList());
		for(File file:filteredFiles){
			LOG.info("Retrieving rerun files:"+file.getAbsolutePath()+" , "+file.length());
		}
		return (List<File>) filteredFiles;
	}
	
	/**
     * Merge rerun json report to original json report
     * e.g. Original run has 5 cases(a,b,c,d,e), among which 3 cases(a,c,d) are failed.
     * In rerun phase, 3 failed cases (a,c,d) are reran, and a,c are passed.
     * In final output json, it will list a,b,c,d,e where only d is failed, and others are passed.
     */
	public static void mergeRerunJsonReportToOriginalReport(String rerunJson,String originalJson) throws FileNotFoundException {
		LOG.info("***************************************************************");
		LOG.info("Merging rerun json file:"+rerunJson+" To "+originalJson);	
	    JsonReportHandler.refineAndRewriteJSONFile(new File(rerunJson),new File(originalJson));
	    LOG.info("***************************************************************");
	    
	}
	
	
    public static void mergeJsonsIntoOne(List<File> files,String targetPath) throws IOException {
        //String targetReport = FileUtils.readFileToString(files.get(0));
        List<File> jsonSrc = new ArrayList<File>();
        for (int i = 0; i < files.size(); i++) {
            System.out.println("Merging json:" + files.get(i).getAbsolutePath() + " TO " + targetPath);
            jsonSrc.add(files.get(i));
        }
        File newFile=new File(targetPath);
        if(!newFile.exists()){
        	newFile.createNewFile();
        }
        mergeJsonFiles(new File(targetPath), jsonSrc);
        //Temp debug
		/*System.out.println("********************************************************************************");
		System.out.println("*****************************Final json start************************************");
		System.out.println("********************************************************************************");
		JSONParser parser = new JSONParser();
		FileReader targetReader=new FileReader(newFile);
		Object targetObj = parser.parse(targetReader);
		JSONArray targetFeatures = (JSONArray)targetObj;
		LOG.info(targetFeatures.toJSONString());
		System.out.println("********************************************************************************");
		System.out.println("*****************************Final json end**************************************");
		System.out.println("********************************************************************************");*/
		//targetReport = FileUtils.readFileToString(files.get(0));
    }
	
	public static void mergeJsonFiles(File jsonTarget, List<File> jsonSource) throws IOException {
        //merge report files		
        for (int i = 0; i < jsonSource.size(); i++) {
            String targetReport = FileUtils.readFileToString(jsonTarget);
            String sourceReport = FileUtils.readFileToString(jsonSource.get(i));
            if(i==0){
            	FileUtils.writeStringToFile(jsonTarget,sourceReport);
            }else{
                FileUtils.writeStringToFile(jsonTarget, targetReport.substring(0, targetReport.length() - 1) + "," + sourceReport.substring(1, sourceReport.length()));
            }
        }
    }

}
