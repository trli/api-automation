package com.salmon.test.framework.helpers.rerun;


import org.testng.annotations.Test;

import com.salmon.test.framework.helpers.Props;

import cucumber.api.testng.AbstractTestNGCucumberTests;

import java.io.FileNotFoundException;

public class BeforeRerun extends AbstractTestNGCucumberTests{	
	@Test(description = "Before rerun cucumber features")
    public void before_rerun() throws FileNotFoundException {
		Props.loadRunConfigProps("/environment.properties");
		if(Props.getEnvProp("OverwriteResultIfRerunPassed").equals("false")){
			System.out.println("Rerun mode switched off, failed scenarios lines in rerun files skinned");
			CucumberRerunHelper.skinAllRerunFiles();
			return;
		}
		System.out.println("***************BEFORE RERUN********************");		
		CucumberRerunHelper.clearRerunFileIfNumberOfFailedScenarioExceedsThreshold();
    }
	
}
