package com.salmon.test.framework.helpers.rerun;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.salmon.test.framework.helpers.Props;
import com.salmon.test.framework.helpers.WebDriverHelper;

public class CucumberRerunHelper {
	private static final Logger LOG = LoggerFactory.getLogger(CucumberRerunHelper.class);	
	/**
     * Verify rerun*.txt to see if number of failed scenarios exceeds threshold set up. 
	 * If yes, make the rerun*.txt blank so that rerun job will not pick any failed scenario from rerun.txt
	 * If no, leave rerun file as it was
     */
	public static void clearRerunFileIfNumberOfFailedScenarioExceedsThreshold() throws FileNotFoundException {
		Props.loadRunConfigProps("/environment.properties");
		int failureThreshold=new Integer(Props.getEnvProp("rerunThreshold"));	
		int totalFailure=countTotalFailureNumber();
		LOG.info("Failed cases got totally:"+totalFailure+"----Expecting not exceeding:"+failureThreshold);		
		if(totalFailure>failureThreshold){
			LOG.info("************************************************************");
			LOG.info("Exceeded threshold:"+failureThreshold+", skinning rerun file");
			LOG.info("************************************************************");
			skinAllRerunFiles();
		}				
	}
	
	
	public static int countTotalFailureNumber() throws FileNotFoundException {	
		boolean isRerunFileFound=false;
		int totalFailure=0;
	    Collection<File> all = new ArrayList<File>();
	    addTree(new File("."), all);
	    for(File file:all){
	    	if(file.getName().toLowerCase().contains("rerun")&&file.getName().endsWith(".txt")&&file.getAbsolutePath().contains("target")){
	    		isRerunFileFound=true;
	    		LOG.info("*******************************************");
	    		LOG.info("Rerun file found:"+file.getAbsolutePath());
	    		printFile(file);
	    		int failedNumber= countRerunFile(file);
	    		LOG.info("Failed number in above rerun file(excluding forced one):"+(failedNumber-1));
	    		totalFailure=totalFailure+failedNumber-1;	    		
	    	}    	
	    }	
	    if(!isRerunFileFound){
	    	LOG.info("Rerun*.txt files not found, please check config of original runners");
	    }
	    return totalFailure;
	}
	
	public static void skinAllRerunFiles() throws FileNotFoundException{
	    Collection<File> all = new ArrayList<File>();
	    addTree(new File("."), all);
	    for(File file:all){
	    	if(file.getName().toLowerCase().contains("rerun")&&file.getName().endsWith(".txt")&&file.getAbsolutePath().contains("target")){
	    		LOG.info("Skinning file:"+file.getAbsolutePath());
	    		skinRerunFile(file);
	    		LOG.info("After skinning:");
	    		printFile(file);
	    	}	    	
	    }		    

	}

	static void addTree(File file, Collection<File> all) {
	    File[] children = file.listFiles();
	    if (children != null) {
	        for (File child : children) {
	            all.add(child);
	            addTree(child, all);
	        }
	    }
	}
	
	static void printFile(File file) throws FileNotFoundException{
		Scanner input = new Scanner(file);
		while (input.hasNextLine())
		{
			LOG.info(input.nextLine());
		}
		input.close();
	}
	
	static int countRerunFile(File file) throws FileNotFoundException{
		Scanner input = new Scanner(file);
		String linesString="";
		while (input.hasNextLine()){
			linesString+=input.nextLine();		  
		}	
		input.close();
		String[] scenarios=linesString.split(" ");
		int scenarioNumber=scenarios.length;
		for(String s:scenarios){
			String[] singleScenario=s.split(":");
			scenarioNumber+=(singleScenario.length-2);
		}		
		LOG.info("Number of scenarios:"+scenarioNumber);
		return scenarioNumber;
	}
	
	static void skinRerunFile(File file) throws FileNotFoundException{
		Scanner input = new Scanner(file);
		String linesString="";
		while (input.hasNextLine()){
			linesString+=input.nextLine();		  
		}	
		input.close();
		String[] scenarios=linesString.split(" ");	
		String newRerunLink="";
		for(String s:scenarios){
			if(s.toLowerCase().contains("forcedfailure")){
				newRerunLink=s;
			}
		}
		PrintWriter writer = new PrintWriter(file);
		if(newRerunLink.equals("")){
			writer.print(scenarios[0]);
		}else{
			writer.print(newRerunLink);
		}		
		writer.close();
		
	}
}
