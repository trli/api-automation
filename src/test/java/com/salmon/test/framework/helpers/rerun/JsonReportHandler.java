package com.salmon.test.framework.helpers.rerun;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.salmon.test.framework.helpers.Props;
import com.salmon.test.framework.helpers.WebDriverHelper;

public class JsonReportHandler {	
	private static final Logger LOG = LoggerFactory.getLogger(JsonReportHandler.class);
	public static void copyJson(File src, File target) throws IOException{	
		JSONParser parser = new JSONParser();
		FileWriter updatedJson = null;
		try{			
			Object srcObj = parser.parse(new FileReader(src));
	        JSONArray srcArray = (JSONArray)srcObj;
	        Object targetObj = parser.parse(new FileReader(target));
	        JSONArray targetArray = (JSONArray)targetObj;
	        targetArray.addAll(srcArray);
        	updatedJson = new FileWriter(target.getAbsolutePath());
        	updatedJson.write(targetArray.toJSONString());
        	updatedJson.flush();  
		} catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally{
        	updatedJson.close();
        } 		
	}		
	
	
	
	//Overwrite original(target) result for passed rerun
	public static void refineAndRewriteJSONFile(File src, File target) throws FileNotFoundException{
		JSONArray srcFeatures=null;
		JSONArray targetFeatures=null;
		JSONParser parser = new JSONParser();
		FileReader srcReader=null;
		FileReader targetReader=null;
		try{		
			srcReader=new FileReader(src);
			targetReader=new FileReader(target);
			Object srcObj = parser.parse(srcReader);
			Object targetObj = parser.parse(targetReader);
			srcFeatures = (JSONArray)srcObj;
			targetFeatures = (JSONArray)targetObj;	 
			LOG.info("***********************************"); 
			LOG.info("Original json: id and result:");  
	        printAllScenarioIDsAndResult(targetFeatures);
	        LOG.info("***********************************"); 
	        LOG.info("Rerun json: id and result:");
			printAllScenarioIDsAndResult(srcFeatures);	
	               
	        //Get a list of rerun(src) scenarios containing id/result/scenario object for each entry
	        List<HashMap<String,Object>> rerunScenarioResults=getAllScenarioIDsAndResult(srcFeatures); 	 
	        //Loop original(target) features
	        for(int i=0;i<targetFeatures.size();i++){
	        	JSONObject feature=(JSONObject)targetFeatures.get(i); 
	        	//Remove result of rerunforcedfailure
	        	if(feature.get("id").toString().toLowerCase().contains("rerunforcedfailure")){
	        		targetFeatures.remove(i);
	        		i--;
	        		continue;
	        	}
	        	if(feature.get("keyword").equals("Feature")){
	        		JSONArray scenarios=(JSONArray) feature.get("elements");
	        		//In case all scenarios in this feature are excluded 
	        		if(scenarios==null){
	        			continue;
	        		}
	        		//Loop original(target) scenarios for each feature
	        		for(int j=0;j<scenarios.size();j++){
	        			JSONObject scenario=(JSONObject)scenarios.get(j);
	        			//Background is standing in line with scenario, but without id attribute, ignore this element
	        			String type=(String) scenario.get("type");	        			
	        			if(!type.equals("scenario")){
	        				continue;
	        			}
	        			String scenarioId=(String) scenario.get("id");		
	    	        	boolean isRerun=false;
	    	        	String tempRerunResult="";
	    	        	JSONObject tempRerunResultObject=null;
	    	        	JSONObject tempRerunBackgroundResultObject=null;
	    	        	//Loop rerun(src) scenario to check if current scenario has been rerun
	    	        	for(int k=0;k<rerunScenarioResults.size();k++){
	    	        		if(scenarioId.equals(rerunScenarioResults.get(k).get("id"))){	    	        			
	    	        			tempRerunResult=(String) rerunScenarioResults.get(k).get("result");
	    	        			tempRerunResultObject=(JSONObject) rerunScenarioResults.get(k).get("scenario");
	    	        			isRerun=true;
	    	        			LOG.info("Find duplicated result:"+scenarioId+" with result:"+tempRerunResult);
	    	        			//Background
	    	        			if(k!=0){
	    	        				tempRerunBackgroundResultObject=(JSONObject) rerunScenarioResults.get(k-1).get("scenario");
	    	        			}
	    	        		}
	    	        	}
	    	        	//If an original scenario is rerun and passed in second run, replace original scenario result
	    	        	if(isRerun&&tempRerunResult.equals("passed")){
	    	        		//LOG.info("Overwriting result for passed rerun: "+scenarioId.replaceAll("-", " ").replaceAll(";", "--"));
	    	        		LOG.info("Overwriting result for passed rerun: "+scenarioId);
	    	        		scenarios.remove(j);
	    	        		scenarios.add(j,tempRerunResultObject);
	    	        		//Replace background result
	    	        		if(j!=0){
	    	        			JSONObject previousScenario=(JSONObject)scenarios.get(j-1);	 
		    	        		String previousType=(String) previousScenario.get("type");	 
		    	        		//Background
			    	        	if(previousType.equals("background")){
			    	        		scenarios.remove(j-1);
			    	        		scenarios.add(j-1,tempRerunBackgroundResultObject);	    	        		
			        				continue;
			        			}
	    	        		}
	    	        	}
	        		}
	        	}
	        } 	 
        }catch (Exception e) {
			e.printStackTrace();
		}
		
		//Writting to new json file
		FileWriter updatedJson;
		try {
			//updatedJson = new FileWriter(target.getAbsolutePath().replaceAll(".json", "refined.json"));
			updatedJson = new FileWriter(target);
			updatedJson.write(targetFeatures.toJSONString());
	    	updatedJson.flush();
	    	updatedJson.close();
		} catch (IOException e) {
			LOG.info("Exception on refining json file");
			e.printStackTrace();
		}finally{
			//LOG.info("Closing src and target files");
			try {
				srcReader.close();
				targetReader.close();				
			} catch (IOException e) {
				LOG.info("Exception on closing src and target files");
				e.printStackTrace();
			}
			
		}
		LOG.info("******************************************************");
		LOG.info("json file after merge - id and result:");  
		printAllScenarioIDsAndResult(targetFeatures);
	}

	public static void deleteFile(String path){			
		File file=new File(path);
		file.deleteOnExit();
	}
	

	
	public static void printScenarioIdsInFeaturesArray(JSONArray features){
		LOG.info("****************************Printing features************************");
		for(int i=0;i<features.size();i++){
        	JSONObject feature=(JSONObject)features.get(i); 
        	LOG.info("Feature id:"+feature.get("id"));
        	if(feature.get("keyword").equals("Feature")){
        		JSONArray scenarios=(JSONArray) feature.get("elements");
        		for(int j=0;j<scenarios.size();j++){
        			JSONObject scenario=(JSONObject)scenarios.get(j); 
        			LOG.info("scenario feature id:"+scenario.get("id"));
        		}
        	}
        } 
	}
	
	public static List<HashMap<String,Object>> getAllScenarioIDsAndResult(JSONArray features){
		//A list of scenarios containing id/result/scenario full content
		List<HashMap<String,Object>> allScenarioResults=new ArrayList<HashMap<String,Object>>();
		for(int i=0;i<features.size();i++){
        	JSONObject feature=(JSONObject)features.get(i); 
        	if(feature.get("keyword").equals("Feature")){
        		JSONArray scenarios=(JSONArray) feature.get("elements");
        		for(int j=0;j<scenarios.size();j++){
        			JSONObject scenario=(JSONObject)scenarios.get(j);
        			String scenarioId=(String) scenario.get("id");
        			String scenarioResult=getResultOfScenario(scenario);
    	        	HashMap<String,Object> scenarioResultMap=new HashMap<String,Object>();
    	        	scenarioResultMap.put("id", scenarioId);
    	        	scenarioResultMap.put("result", scenarioResult);
    	        	scenarioResultMap.put("scenario", scenario);
    	        	allScenarioResults.add(scenarioResultMap);
        		}
        	}
        } 							
		return allScenarioResults;
	}
	
	public static void printAllScenarioIDsAndResult(JSONArray features) {
		LOG.info("Printing results of features:");
		new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < features.size(); i++) {
			JSONObject feature = (JSONObject) features.get(i);
			if (feature.get("keyword").equals("Feature")) {
				JSONArray scenarios = (JSONArray) feature.get("elements");
				//in case all scenarios in feature has been excluded
				if(scenarios==null){
					continue;
				}
				for (int j = 0; j < scenarios.size(); j++) {
					JSONObject scenario = (JSONObject) scenarios.get(j);
					String scenarioId = (String) scenario.get("id");
					if(scenarioId==null){
						scenarioId=(String) scenario.get("keyword");
					}
					String scenarioResult = getResultOfScenario(scenario);
					LOG.info(scenarioId+" "+scenarioResult);					
				}
			}
		}
	}
	
	public static String getResultOfScenario(JSONObject scenario){
		String result="passed";
		String beforeResultString;
		String afterResultString;
		try{
			JSONArray beforeArray=(JSONArray)scenario.get("before");	
			JSONObject before=(JSONObject)beforeArray.get(0);
			JSONObject beforeResult=(JSONObject)before.get("result");
			beforeResultString=(String) beforeResult.get("status");	
		}catch(Exception e){
			beforeResultString="passed";
		}		
		JSONArray stepsArray=(JSONArray)scenario.get("steps");
		String stepsResultString="passed";
		for(int i=0;i<stepsArray.size();i++){
			JSONObject step=(JSONObject)stepsArray.get(i); 
			JSONObject stepsResult=(JSONObject)step.get("result");
			String stepResultString=(String) stepsResult.get("status");
			if(!stepResultString.equals("passed")){
				stepsResultString="failed";
			}
		}				
		try{
			JSONArray afterArray=(JSONArray)scenario.get("after");
			JSONObject after=(JSONObject)afterArray.get(0); 
			JSONObject afterResult=(JSONObject)after.get("result");
			afterResultString=(String) afterResult.get("status");
		}catch(Exception e){
			afterResultString="passed";
		}							
		if(beforeResultString.equalsIgnoreCase("failed")|stepsResultString.equalsIgnoreCase("failed")|afterResultString.equalsIgnoreCase("failed")){
			result="failed";
		}else{
			result="passed";
		}
		return result;		
	}
}
