package com.salmon.test.framework.helpers.screenshot_helper;

import com.salmon.test.framework.helpers.WebDriverHelper;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;

import static com.salmon.test.framework.helpers.WebDriverHelper.getWebDriver;

public class ScreenshotHook {

    private static final Logger LOG = LoggerFactory.getLogger(ScreenshotHook.class);

    //Not Execute for any api or mobile tests
    @After
    public void embedScreenshot(Scenario scenario) {
        Collection<String> tags = scenario.getSourceTagNames();
        if (tags.contains("@api") || tags.contains("@database")||tags.contains("@nowebdriver")) {
            return;
        }
        try {
            Map<String, Object> screenShots = ScreenshotHelper.getScreenShotsForCurrentTest();
            for (Map.Entry<String, Object> screenShot : screenShots.entrySet()) {
                scenario.write(screenShot.getKey());
                scenario.embed((byte[]) screenShot.getValue(), "image/png");
            }

            ScreenshotHelper.tidyUpAfterTestRun();

            if (scenario.isFailed()) {
                scenario.write(WebDriverHelper.getWebDriver().getCurrentUrl());
                byte[] screenShot = ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenShot, "image/png");
            }

        } catch (WebDriverException | ClassCastException wde) {
            LOG.error(wde.getMessage());
        } finally {
            getWebDriver().quit();
            WebDriverHelper.REAL_DRIVER = null;
            //getWebDriver().switchTo().defaultContent();
        }
    }
}
