package com.salmon.test.framework.helpers.rerun;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.IHookCallBack;
import org.testng.ITestResult;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.salmon.test.framework.helpers.Props;

import cucumber.api.testng.AbstractTestNGCucumberTests;
import cucumber.api.testng.TestNGCucumberRunner;

public class AfterRerun extends AbstractTestNGCucumberTests{
	
	@Test(description = "After rerun cucumber features")
    public void after_run_cukes1() throws IOException {
		Props.loadRunConfigProps("/environment.properties");
		if(Props.getEnvProp("OverwriteResultIfRerunPassed").equals("false")){
			System.out.println("Rerun mode switched off, skipping after rerun");
			List<File> allJsonFiles=CucumberJsonReportHelper.getOriginalJsonFiles("target/cucumber-report", "json", "cucumber.json");
			CucumberJsonReportHelper.mergeJsonsIntoOne(allJsonFiles,"target/cucumber-report/cucumber.json");
			CucumberJsonReportHelper.clearJsonFiles(allJsonFiles);
			return;
		}
		
		System.out.println("***************AFTER RERUN********************");
		List<File> rerunJsonFiles=CucumberJsonReportHelper.getRerunJsonFiles("target/cucumber-report", "json", "cucumber.json");
		List<File> originalJsonFiles=CucumberJsonReportHelper.getOriginalJsonFiles("target/cucumber-report", "json", "cucumber.json");
		for(int i=0;i<originalJsonFiles.size();i++){
			CucumberJsonReportHelper.mergeRerunJsonReportToOriginalReport(rerunJsonFiles.get(i).getAbsolutePath(), originalJsonFiles.get(i).getAbsolutePath());
		}		
		CucumberJsonReportHelper.clearJsonFiles(rerunJsonFiles);	
		CucumberJsonReportHelper.mergeJsonsIntoOne(originalJsonFiles,"target/cucumber-report/cucumber.json");
		CucumberJsonReportHelper.clearJsonFiles(originalJsonFiles);
    }
}
