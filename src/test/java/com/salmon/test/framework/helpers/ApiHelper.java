package com.salmon.test.framework.helpers;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.salmon.test.util.StringParser;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import static com.jayway.restassured.RestAssured.given;

/**
 * Every feature api class should extend this class. e.g. CreateOrderAPI
 */

public class ApiHelper {
    private static final String expiredToken="Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6ImRmOGQ5ZWU0MDNiY2M3MTg1YWQ1MTA0MTE5NGJk\n" +
            "MzQzMzc0MmQ5YWEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb" +
            "20iLCJhenAiOiIzMjU1NTk0MDU1OS5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImF1ZCI6IjMyN" +
            "TU1OTQwNTU5LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTA5NDMwMTczNjQ4MzE3N" +
            "TI4MjM3IiwiZW1haWwiOiJ0cmxpQHd1bmRlcm1hbmNvbW1lcmNlLmNvbSIsImVtYWlsX3ZlcmlmaWVkI" +
            "jp0cnVlLCJhdF9oYXNoIjoia2pSZlR1Z29FN1Rjc0h2bGlxYjNnZyIsImlhdCI6MTU2NjE5NDczMCwiZ" +
            "XhwIjoxNTY2MTk4MzMwfQ.meagRooDG0rRhLo7A56Ks1OIZm9Qga2LczajrqCoVDV7qhIh3oKYbR492O" +
            "Vt9eTKTi1dnhYQlfqz2RNTI1UYXrj3Wq61qqPsJh1llu5UA_H5LqBx4t1paG15UViWR_OuBtK0lIg62R" +
            "ln_Ap7FFXERiVULVx6GMWZJtc8VEYmcmVwHSWelkT5Vcy_xvzCl4ro0mNTyBJQ4mKYcHHeWpW4TPuNpd" +
            "pgj-cbMoSWFUyCKJPpJOuDW2yTKj7-zMmk7qFq5R_CMRuxXu9BLgZzgqa3B5g6D3wV6av78m4R0ZC1rh" +
            "tKsvPEp-1giKIyIAM64g4qWDn_7h3PDItizJxu08V2UQ";
    private static final String invalid="ya29.GlxpB1YSjq5utjCswNKC30Nc_amdF9yVLJ_6K7lP_hpQhyB8JI57DAEKVcqh6oujwgOAsoPlFto0FXKvD-mRzsTgVDXy6Hh6dcN_3Bt84GvjZzm1aFNLYZDcNJuRRg";
    private static String validToken;
    {
        validToken=Props.getEnvProp("valid_id_token");
    }

    public static RequestSpecification givenConfig() {
        RestAssured.useRelaxedHTTPSValidation();
        return given().log().all().
                header("Content-Type", "application/json").header("Authorization",validToken)
                .urlEncodingEnabled(false);
    }

    public static RequestSpecification givenConfigWithToken(String token) {
        RestAssured.useRelaxedHTTPSValidation();
        return given().log().all().
                header("Content-Type", "application/json").header("Authorization",token)
                .urlEncodingEnabled(false);
    }

    public String getJsonByName(String path) throws ParseException {
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();
        Object obj=new Object();
        try {
            FileReader reader = new FileReader(path);
            //Read JSON file
            obj = jsonParser.parse(reader);
            System.out.println(obj);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }

    public static String getValueInBody(String body, String key){
        String value=StringParser.parseAndGetValueOf(body, ",", ":",key,new String[]{"\\{","}","\""," "});
        System.out.println("getting"+value);
        return value;
    }

}
