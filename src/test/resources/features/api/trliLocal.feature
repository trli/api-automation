@apilocal @api
Feature: Create order - Positive - As a user, I want to fire a request to create order api and see successful message returned in response
  Scenario: Create order api - I want to fire a request to create order api and see successful message returned in response
    Given the user requests for valid token to service
    #When the user calls create order api using valid json
    #Then the response contains successful create order message


#  Scenario Outline: COL-3311 User creates a single list and adds items - using payload to add items in one hit
#    Given I create a list "<list identifier>" with description "<description>" for shop type "<shop type>" with "<number of items>" items in the payload
#    When I call the API to get the users list
#    Then the response contains all the users lists
#    When I call get list by type CUSTOMER for shop type "<shop type>"
#    Then the response contains all products for "<shop type>" for list type "CUSTOMER"
#    Examples:
#      | list identifier | description   | shop type | number of items |
#      | identifier1C    | description1  | CLPBE     | 1               |
#      | identifier1B    | description1  | BIOBE     | 1               |
#      | identifier2C    | description1  | CLPBE     | 4               |
#      | identifier2B    | description1  | CLPBE     | 5               |